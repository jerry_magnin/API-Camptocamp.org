#coding: utf-8

"""
Script de conversion de données de l’API C2C en GPX. En attendant que l’export
GPX soit disponible.

author: Jerry Magnin <dev@montagnisme.fr>
licence: GPL V3 or later

Installer les paquets manquants:
pip3 install gpxpy
apt-get install python3-pyproj
"""
import requests
import json
import gpxpy as g

from gpxpy import gpx
from pyproj import Proj, transform

# Config -> modifier ces deux variables pour changer la requête et le fichier de sortie
url = 'https://api.camptocamp.org/routes/53933'
filename = 'replomb.gpx'

inProj = Proj(init='epsg:3857')
outProj = Proj(init='epsg:4326')

resp = requests.get(url)
a = json.loads(resp.text)

#print(a['geometry']['geom_detail']['coordinates'])
b = a['geometry']['geom_detail']
b = json.loads(b)
# print(b['coordinates'][0][0][2])

# creating file
mygpx = gpx.GPX()
track = gpx.GPXTrack()
mygpx.tracks.append(track)

segment = gpx.GPXTrackSegment()
track.segments.append(segment)

# Create points
coords = b['coordinates']
# if len(coords) is 1, we have a very nested list (can occur -> route 53933)
if len(coords) == 1:
    for point in coords[0]:
        x, y = transform(inProj, outProj, point[0], point[1])
        segment.points.append(gpx.GPXTrackPoint(y, x, elevation=point[2]))

else:
    # less nested list, need to remove one level
    for point in coords:
        x, y = transform(inProj, outProj, point[0], point[1])
        segment.points.append(gpx.GPXTrackPoint(y, x, elevation=point[2]))



with open(filename, 'w') as outFile:
    outFile.write(mygpx.to_xml())

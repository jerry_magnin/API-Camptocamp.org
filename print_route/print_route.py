#! /usr/bin/python3
#! coding: utf-8

import jinja2
import os
from formatLaTeX import *
import json
import argparse
from jinja2 import Template
latex_jinja_env = jinja2.Environment(
	block_start_string = '\BLOCK{',
	block_end_string = '}',
	variable_start_string = '\VAR{',
	variable_end_string = '}',
	comment_start_string = '\#{',
	comment_end_string = '}',
	line_statement_prefix = '%%',
	line_comment_prefix = '%#',
	trim_blocks = True,
	autoescape = False,
	loader = jinja2.FileSystemLoader(os.path.abspath('.'))
)

parser = argparse.ArgumentParser()
parser.add_argument('url', type=str, help='URL à parser')
parser.add_argument('out', type=str, help='Fichier en sortie')

args = parser.parse_args()

url = args.url
outputFile = args.out

url = convert_url(url)
data = get_route_json(url)

#data_json = json.loads(data)
descr = extract_description(data)
images = get_inline_images_ID(descr['description'])
imagesList = []

for image in images:
    imgJsn = get_image_json(image)
    imgInfos = get_image_infos(imgJsn)
    imagesList.append(download_image(imgInfos['filename'], image))

description = md2LaTeX(descr['description'])
remarks = md2LaTeX(descr['remarks'])

template = latex_jinja_env.get_template('c2c.tex')
rendered = template.render(title=descr['title'], description=description, remarks=remarks)

with open(outputFile, 'w') as out:
    out.write(rendered)

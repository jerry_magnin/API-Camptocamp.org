#!/bin/zsh

cd Get_route_list
cp ../GetC2CData-LO.py Scripts/python/GetC2CData.py
zip -0 -X ../document.ods mimetype
zip -r ../document.ods * -x mimetype

cd ..

rm Dist/Get_C2C_routes.ods
mv document.ods Dist/Get_C2C_routes.ods
